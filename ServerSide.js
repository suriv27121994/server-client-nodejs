// const createServer = require("http-server");
// const Server = require("socket.io");
import {createServer} from "http";
import  {Server}  from "socket.io";

const httpServer = createServer();
const io = new Server(httpServer, {});

io.on("Connection", (socket) => {
  socket.on("Message", (msg) => {
    console.log(msg);

    // The send it to the server ONLY
    socket.emit("Message", "You sent a message");

    // The send it to all the clients EXCEPT THE SENDER
    socket.broadcast.emit("Message", "You got a new message");

    // Send it to All Clients
    io.emit("ANY_TEXT", "ANY_DATA_YOU_WANT_TO_SEND");
  });
});
httpServer.listen(3000);
